# ubnt-eos-platform

## Introduction

`ubnt-eos-platform` is a dump of the Ubiquiti EdgeOS platform source. It includes sources for the Cavium/Ubiquiti GPL kernel modules and some userspace tools.

## Origin

This can in principle be obtained from the Ubiquiti downloads for EdgeOS v2.0 GPL archives. This particular extract was obtained from the ERLite-3[1] v2.0.6 archive[2]:

```shell
$ sha256sum GPL.ER-e100.v2.0.6.5208553.tar.bz2 
b4a83f5624504dca29644c50075e95e92c0ccae45b5eff419ee6e88747d64a22  GPL.ER-e100.v2.0.6.5208553.tar.bz2
```

Inside this archive is the `ubnt-platform` source archive:

```shell
$ tar -xf GPL.ER-e100.v2.0.6.5208553.tar.bz2 source/ubnt-platform_0.1.17+t5211023.dev.stretch.9cddb692.tar.gz
$ sha256sum source/ubnt-platform_0.1.17+t5211023.dev.stretch.9cddb692.tar.gz 
c933679adef980550e92d4c8d8b01752626509f2d05f34de545df0014474d316  source/ubnt-platform_0.1.17+t5211023.dev.stretch.9cddb692.tar.gz
```

## Contents

Inside the `ubnt-platform` archive, you will find:

- `cavium-ipfwd-offload`: Cavium Octeon IP Forwarding kernel module `cavium_ip_offload`
- `cavium_ipsec_kame`: Cavium IPSec kernel module `cvm_ipsec_kame`
- `ebtables`: Userspace `ebtables` utility
- `ER-e*`: Makefiles and symlinks for the various EdgeRouter models
- `iproute2*`: Userspace `iproute2` utilities
- `ipt-netflow`: iptables NETFLOW target kernel module `ipt_NETFLOW`/`ip6t_NETFLOW`[3]
- `rtsp-linux`: RTSP connection tracking and NAT kernel modules `nf_conntrack_rtsp` and `nf_nat_rtsp`, respectively[4]
- `ubnt_nf_app`: Ubiquiti netfilter application for Deep Packet Inspection (DPI)

Of particular note are the Cavium kernel modules, `cavium_ip_offload` and `cvm_ipsec_kame`, which are used for hardware offloading[5] on the Cavium Octeon platform. Presumably, these should work with the Octeon+/Octeon II/Octeon III CPUs.

## Installation

To install, simply clone this repo:

```shell
git clone https://gitlab.com/mkrupcale/ubnt-eos-platform.git
cd ubnt-eos-platform
```

## Usage

The kernel modules and tools can in principle be compiled with the Cavium Octeon SDK `mips64-octeon-linux-gnu` toolchain, although I have not tested this.

Instead, my intention is to try to upstream the Cavium hardware offloading Linux kernel modules so that mainline Linux can use all of the Cavium Octeon hardware features.

## License

The contents of this repository are licensed under the GPLv2 license, except where otherwise noted.

## References

1. [Ubiquiti EdgeRouter Lite](https://www.ui.com/edgemax/edgerouter-lite/)
2. [Ubiquiti - Downloads - ERLite-3 v2.0.6](https://www.ui.com/download/edgemax/edgerouter-lite/erlite3/edgerouter-erlite-3erpoe-5-firmware-v206#)
3. [`aabc/ipt-netflow`](https://github.com/aabc/ipt-netflow)
4. [`maru-sama/rtsp-linux`](https://github.com/maru-sama/rtsp-linux)
5. [Ubiquiti - EdgeRouter - Hardware Offloading](https://help.ubnt.com/hc/en-us/articles/115006567467-EdgeRouter-Hardware-Offloading)
