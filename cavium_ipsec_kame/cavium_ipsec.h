/***************************************************************************
 * Copyright (c) 2003 - 2012 Cavium Networks (support@cavium.com). All rights
 * reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *
 *     * Neither the name of Cavium Networks nor the names of
 *       its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written
 *       permission.
 *
 * This Software, including technical data, may be subject to U.S. export
 * control laws, including the U.S. Export Administration Act and its
 * associated regulations, and may be subject to export or import regulations
 * in other countries. You warrant that You will comply strictly in all
 * respects with all such regulations and acknowledge that you have the
 * responsibility to obtain licenses to export, re-export or import the
 * Software.
 * 
 * TO THE MAXIMUM EXTENT PERMITTED BY LAW, THE SOFTWARE IS PROVIDED "AS IS"
 * AND WITH ALL FAULTS AND CAVIUM NETWORKS MAKES NO PROMISES, REPRESENTATIONS
 * OR WARRANTIES, EITHER EXPRESS, IMPLIED, STATUTORY, OR OTHERWISE, WITH
 * RESPECT TO THE SOFTWARE, INCLUDING ITS CONDITION, ITS CONFORMITY TO ANY
 * REPRESENTATION OR DESCRIPTION, OR THE EXISTENCE OF ANY LATENT OR PATENT
 * DEFECTS, AND CAVIUM SPECIFICALLY DISCLAIMS ALL IMPLIED (IF ANY) WARRANTIES
 * OF TITLE, MERCHANTABILITY, NONINFRINGEMENT, FITNESS FOR A PARTICULAR
 * PURPOSE, LACK OF VIRUSES, ACCURACY OR COMPLETENESS, QUIET ENJOYMENT, QUIET
 * POSSESSION OR CORRESPONDENCE TO DESCRIPTION.  THE ENTIRE RISK ARISING OUT
 * OF USE OR PERFORMANCE OF THE SOFTWARE LIES WITH YOU.
 *
 ****************************************************************************/

#ifndef _CAVIUM_IPSEC_H_
#define _CAVIUM_IPSEC_H_

#include <net/xfrm.h>
#include <net/esp.h>
#include <net/ah.h>
#include <linux/list.h>



typedef int (*cavium_ipsec_fn)(uint8_t *, uint8_t *, uint32_t, uint8_t *,
                               int, uint8_t *,
                               uint64_t *, uint64_t *);

typedef int (*cavium_process_fn)(struct xfrm_state *, struct sk_buff *, int);

typedef struct {
   uint8_t inner_hash[24];
   uint8_t outer_hash[24];
   cavium_ipsec_fn oct_fn;
   cavium_process_fn process_fn;
	struct xfrm_state *xfrm; /* back pointer to the xfrm_state */
	struct list_head list;
} oct_data_t;

/* Wrapper APIs */
void * cavium_alloc_n_fill(struct xfrm_state *x);
void cavium_delete_hndl(struct xfrm_state *x);

/* IPSec APIs */
void cav_calc_hash(int sha1, uint64_t *key, uint8_t *inner, uint8_t *outer);

int AES_CBC_sha1_encrypt(uint8_t *input, uint8_t *output, uint32_t pktlen, 
                         uint8_t *aes_key, int aes_key_len, uint8_t *aes_iv,
                         uint64_t *start_inner_sha, uint64_t *start_outer_sha);
int AES_CBC_sha1_decrypt(uint8_t *input, uint8_t *output, uint32_t pktlen, 
                         uint8_t *aes_key, int aes_key_len, uint8_t *aes_iv,
                         uint64_t *start_inner_sha, uint64_t *start_outer_sha);
int DES_CBC_sha1_encrypt (uint8_t *input, uint8_t *output, uint32_t pktlen, 
                          uint8_t *des_key, int des_key_len, uint8_t *des_iv, 
								  uint64_t *start_inner_sha, uint64_t *start_outer_sha);
int DES_CBC_sha1_decrypt (uint8_t *input, uint8_t *output, uint32_t pktlen, 
                          uint8_t *des_key, int des_key_len, uint8_t *des_iv, 
								  uint64_t *start_inner_sha, uint64_t *start_outer_sha);
int AES_CBC_md5_encrypt(uint8_t *input, uint8_t *output, uint32_t pktlen, 
                        uint8_t *aes_key, int aes_key_len, uint8_t *aes_iv,
                        uint64_t *start_inner_md5, uint64_t *start_outer_md5);
int AES_CBC_md5_decrypt(uint8_t *input, uint8_t *output, uint32_t pktlen, 
                        uint8_t *aes_key, int aes_key_len, uint8_t *aes_iv,
                        uint64_t *start_inner_md5, uint64_t *start_outer_md5);
int DES_CBC_md5_encrypt(uint8_t *input, uint8_t *output, uint32_t pktlen, 
                        uint8_t *des_key, int des_key_len, uint8_t *des_iv, 
								uint64_t *start_inner_md5, uint64_t *start_outer_md5);
int DES_CBC_md5_decrypt (uint8_t *input, uint8_t *output, uint32_t pktlen, 
                         uint8_t *des_key, int des_key_len, uint8_t *des_iv, 
								 uint64_t *start_inner_md5, uint64_t *start_outer_md5);
int AH_sha1(uint8_t *input, uint8_t *output, uint32_t pktlen, 
            uint8_t *unused1, int unused2, uint8_t *unused3, 
            uint64_t *start_inner_sha, uint64_t *start_outer_sha);
int AH_md5(uint8_t *input, uint8_t *output, uint32_t pktlen, 
           uint8_t *unused1, int unused2, uint8_t *unused3, 
           uint64_t *start_inner_md5, uint64_t *start_outer_md5);
int NULL_sha1_encrypt(uint8_t *input, uint8_t *output, uint32_t pktlen, 
           			    uint8_t *unused1, int unused2, uint8_t *unused3, 
                      uint64_t *start_inner_sha, uint64_t *start_outer_sha);
int NULL_sha1_decrypt(uint8_t *input, uint8_t *output, uint32_t pktlen, 
           			    uint8_t *unused1, int unused2, uint8_t *unused3, 
                      uint64_t *start_inner_sha, uint64_t *start_outer_sha);
int NULL_md5_encrypt(uint8_t *input, uint8_t *output, uint32_t pktlen, 
           			   uint8_t *unused1, int unused2, uint8_t *unused3, 
                     uint64_t *start_inner_md5, uint64_t *start_outer_md5);
int NULL_md5_decrypt(uint8_t *input, uint8_t *output, uint32_t pktlen, 
           			   uint8_t *unused1, int unused2, uint8_t *unused3, 
                     uint64_t *start_inner_md5, uint64_t *start_outer_md5);
int AES_CBC_encrypt(uint8_t *input, uint8_t *output, uint32_t pktlen, 
                    uint8_t *aes_key, int aes_key_len, uint8_t *aes_iv,
                    uint64_t *unused1, uint64_t *unused2);
int AES_CBC_decrypt(uint8_t *input, uint8_t *output, uint32_t pktlen, 
                    uint8_t *aes_key, int aes_key_len, uint8_t *aes_iv,
                    uint64_t *unused1, uint64_t *unused2);
int DES_CBC_encrypt(uint8_t *input, uint8_t *output, uint32_t pktlen, 
                    uint8_t *des_key, int des_key_len, uint8_t *des_iv, 
                    uint64_t *unused1, uint64_t *unused2);
int DES_CBC_decrypt (uint8_t *input, uint8_t *output, uint32_t pktlen, 
                     uint8_t *des_key, int des_key_len, uint8_t *des_iv, 
                     uint64_t *unused1, uint64_t *unused2);

#endif /* _CAVIUM_IPSEC_H_ */
