/***************************************************************************
 * Copyright (c) 2003 - 2012 Cavium Networks (support@cavium.com). All rights
 * reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *
 *     * Neither the name of Cavium Networks nor the names of
 *       its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written
 *       permission.
 *
 * This Software, including technical data, may be subject to U.S. export
 * control laws, including the U.S. Export Administration Act and its
 * associated regulations, and may be subject to export or import regulations
 * in other countries. You warrant that You will comply strictly in all
 * respects with all such regulations and acknowledge that you have the
 * responsibility to obtain licenses to export, re-export or import the
 * Software.
 * 
 * TO THE MAXIMUM EXTENT PERMITTED BY LAW, THE SOFTWARE IS PROVIDED "AS IS"
 * AND WITH ALL FAULTS AND CAVIUM NETWORKS MAKES NO PROMISES, REPRESENTATIONS
 * OR WARRANTIES, EITHER EXPRESS, IMPLIED, STATUTORY, OR OTHERWISE, WITH
 * RESPECT TO THE SOFTWARE, INCLUDING ITS CONDITION, ITS CONFORMITY TO ANY
 * REPRESENTATION OR DESCRIPTION, OR THE EXISTENCE OF ANY LATENT OR PATENT
 * DEFECTS, AND CAVIUM SPECIFICALLY DISCLAIMS ALL IMPLIED (IF ANY) WARRANTIES
 * OF TITLE, MERCHANTABILITY, NONINFRINGEMENT, FITNESS FOR A PARTICULAR
 * PURPOSE, LACK OF VIRUSES, ACCURACY OR COMPLETENESS, QUIET ENJOYMENT, QUIET
 * POSSESSION OR CORRESPONDENCE TO DESCRIPTION.  THE ENTIRE RISK ARISING OUT
 * OF USE OR PERFORMANCE OF THE SOFTWARE LIES WITH YOU.
 *
 ****************************************************************************/

#include <crypto/aead.h>
#include <crypto/authenc.h>
#include <crypto/rng.h>
#include <linux/kernel.h>
#include <linux/crypto.h>
#include <net/xfrm.h>
#include <net/esp.h>
#include <net/ah.h>
#include <net/netlink.h>
#include <linux/netfilter.h>
#include <uapi/linux/in6.h>
#include <linux/netfilter_ipv4.h>
#include <net/ip.h>
#include <net/addrconf.h>
#include <net/ipv6.h>
#include "cavium_ipsec.h"
#include "octeon-asm.h"

extern spinlock_t cav_sahdl_lock;
extern struct list_head cav_sahdl_list;

#define CAV_INSERT_IN_GLOB_LIST(n)  { \
   spin_lock_bh(&cav_sahdl_lock); \
   list_add_tail(&(((oct_data_t *)(n))->list),&cav_sahdl_list); \
   spin_unlock_bh(&cav_sahdl_lock); \
}

#define CAV_DEL_FROM_GLOB_LIST(n) { \
   spin_lock_bh(&cav_sahdl_lock); \
   list_del(&(((oct_data_t *)(n))->list)); \
   spin_unlock_bh(&cav_sahdl_lock); \
}
static int
cavium_process_esp_pkt(void *sa_handle, u8* iv, struct sk_buff *skb)
{
   oct_data_t *d = (oct_data_t *) (sa_handle);
   int ekeylen = (d->xfrm->ealg->alg_key_len+7)/8;
   uint8_t *ekey = d->xfrm->ealg->alg_key;
 
   if (unlikely(d == NULL || d->oct_fn == NULL)) {
      return -EIO;
   }
   /* Will be used in all the encrypt functions */
   CVMX_PREFETCH0(ekey);

   return (d->oct_fn(skb->data, skb->data, skb->len, ekey, ekeylen,
            iv,
            (uint64_t *) (d->inner_hash),
            (uint64_t *) (d->outer_hash)));
}

static int
cavium_process_ah_pkt(void *sa_handle, struct ah_data *ah,
                      struct sk_buff *skb, uint8_t * out)
{
   oct_data_t *d = (oct_data_t *) (sa_handle);
   if (unlikely(d == NULL || d->oct_fn == NULL)) {
      return -EIO;
   }

   *(uint64_t *) out = 0;
   *(uint32_t *) (out + 8) = 0;
   return (d->oct_fn(skb->data, out, skb->len, NULL, 0, NULL,
            (uint64_t *) (d->inner_hash),
            (uint64_t *) (d->outer_hash)));
}

int cavium_ipsec_esp4_output(struct xfrm_state *x, struct sk_buff *skb, int offset)
{
	int err;
	struct ip_esp_hdr *esph;
	struct crypto_aead *aead;
	struct sk_buff *trailer;
	u8 *tail;
	int blksize;
	int clen;
	int alen;
	int nfrags;
	
	
	/* skb is pure payload to encrypt */
	
	err = -ENOMEM;

	/* Round to block size */
	clen = skb->len;

	aead = x->data;
	alen = crypto_aead_authsize(aead);
        
	blksize = ALIGN(crypto_aead_blocksize(aead), 4);
	clen = ALIGN(clen + 2, blksize);

	if ((err = skb_cow_data(skb, clen - skb->len + alen, &trailer)) < 0)
		goto error;
	nfrags = err;

	/* Fill padding... */
	tail = skb_tail_pointer(trailer);
	do {
		int i;
		for (i=0; i<clen-skb->len - 2; i++)
			tail[i] = i + 1;
	} while (0);
	tail[clen - skb->len - 2] = (clen - skb->len) - 2;
	tail[clen - skb->len - 1] = *skb_mac_header(skb);
	pskb_put(skb, trailer, clen - skb->len);

	skb_push(skb, -skb_network_offset(skb));
	esph = ip_esp_hdr(skb);
	*skb_mac_header(skb) = IPPROTO_ESP;

	/* this is non-NULL only with UDP Encapsulation */
	if (x->encap) {
		struct xfrm_encap_tmpl *encap = x->encap;
		struct udphdr *uh;
		__be32 *udpdata32;
		__be16 sport, dport;
		int encap_type;

		spin_lock_bh(&x->lock);
		sport = encap->encap_sport;
		dport = encap->encap_dport;
		encap_type = encap->encap_type;
		spin_unlock_bh(&x->lock);

		uh = (struct udphdr *)esph;
		uh->source = sport;
		uh->dest = dport;
		/* Add the authentication header length as well in UDP length */
		uh->len = htons(skb->len - skb_transport_offset(skb) + alen);
		uh->check = 0;

		switch (encap_type) {
		default:
		case UDP_ENCAP_ESPINUDP:
			esph = (struct ip_esp_hdr *)(uh + 1);
			break;
		case UDP_ENCAP_ESPINUDP_NON_IKE:
			udpdata32 = (__be32 *)(uh + 1);
			udpdata32[0] = udpdata32[1] = 0;
			esph = (struct ip_esp_hdr *)(udpdata32 + 2);
			break;
		}

		*skb_mac_header(skb) = IPPROTO_UDP;
	}

	esph->spi = x->id.spi;
	esph->seq_no = htonl(XFRM_SKB_CB(skb)->seq.output.low);

	if(crypto_aead_ivsize(aead)) {
		if (unlikely(!x->ivinitted)) {
			err = crypto_rng_get_bytes(crypto_default_rng, x->iv,
                                   crypto_aead_ivsize(aead));
		        if(err)
           		   return err;
                        x->ivinitted = 1; 
                }
        }

      {	
        int ret;
      __skb_pull(skb, (unsigned char *) esph - skb_network_header(skb));
      if (crypto_aead_ivsize(aead)) 
         memcpy(esph->enc_data, x->iv,crypto_aead_ivsize(aead));
      
      ret = cavium_process_esp_pkt(x->sa_handle, x->iv, skb);
	err = ret;
      __skb_push(skb, (unsigned char *) esph - skb_network_header(skb));
      if(likely(!ret)) {
	 if(x->aalg){
	  struct xfrm_algo_desc  *aalg_desc;
	  aalg_desc = xfrm_aalg_get_byname(x->aalg->alg_name, 0);
        if (aalg_desc->uinfo.auth.icv_fullbits/8) 
             skb_put(skb, alen);	
       } 
       }
      if (unlikely(ret != -EIO))
         goto error;
}
error:
	return err;
}
int cavium_ipsec_esp6_output(struct xfrm_state *x, struct sk_buff *skb, int offset)
{
	int err;
	struct ip_esp_hdr *esph;
	struct crypto_aead *aead;
	struct sk_buff *trailer;
	u8 *tail;
	int blksize;
	int clen;
	int alen;
	int nfrags;
	
	
	/* skb is pure payload to encrypt */
	
	err = -ENOMEM;

	/* Round to block size */
	clen = skb->len;

	aead = x->data;
	alen = crypto_aead_authsize(aead);
        
	blksize = ALIGN(crypto_aead_blocksize(aead), 4);
	clen = ALIGN(clen + 2, blksize);

	if ((err = skb_cow_data(skb, clen - skb->len + alen, &trailer)) < 0)
		goto error;
	nfrags = err;

	/* Fill padding... */
	tail = skb_tail_pointer(trailer);
	do {
		int i;
		for (i=0; i<clen-skb->len - 2; i++)
			tail[i] = i + 1;
	} while (0);
	tail[clen - skb->len - 2] = (clen - skb->len) - 2;
	tail[clen - skb->len - 1] = *skb_mac_header(skb);
	pskb_put(skb, trailer, clen - skb->len);

	skb_push(skb, -skb_network_offset(skb));
	esph = ip_esp_hdr(skb);
	*skb_mac_header(skb) = IPPROTO_ESP;

	esph->spi = x->id.spi;
	esph->seq_no = htonl(XFRM_SKB_CB(skb)->seq.output.low);

	if(crypto_aead_ivsize(aead)) {
		if (unlikely(!x->ivinitted)) {
			err = crypto_rng_get_bytes(crypto_default_rng, x->iv,
                                   crypto_aead_ivsize(aead));
		        if(err)
           		   return err;
                        x->ivinitted = 1; 
                }
        }

      {	
        int ret;
      __skb_pull(skb, (unsigned char *) esph - skb_network_header(skb));
      if (crypto_aead_ivsize(aead)) 
         memcpy(esph->enc_data, x->iv,crypto_aead_ivsize(aead));
      
      ret = cavium_process_esp_pkt(x->sa_handle, x->iv, skb);
	err = ret;
      __skb_push(skb, (unsigned char *) esph - skb_network_header(skb));
      if(likely(!ret)) {
	 if(x->aalg){
	  struct xfrm_algo_desc  *aalg_desc;
	  aalg_desc = xfrm_aalg_get_byname(x->aalg->alg_name, 0);
        if (aalg_desc->uinfo.auth.icv_fullbits/8) 
             skb_put(skb, alen);	
       } 
       }
      if (unlikely(ret != -EIO))
         goto error;
}
error:
	return err;
}
static int esp4_input_done2(struct sk_buff *skb, int err)
{
	struct iphdr *iph;
	struct xfrm_state *x = xfrm_input_state(skb);
	struct crypto_aead *aead = x->data;
	int alen = crypto_aead_authsize(aead);
	int hlen = sizeof(struct ip_esp_hdr) + crypto_aead_ivsize(aead);
	int elen = skb->len - hlen;
	int ihl;
	u8 nexthdr[2];
	int padlen;


	if (unlikely(err))
		goto out;

	if (skb_copy_bits(skb, skb->len-alen-2, nexthdr, 2))
		BUG();

	err = -EINVAL;
	padlen = nexthdr[0];
	if (padlen + 2 + alen >= elen) 
		goto out;

	/* ... check padding bits here. Silly. :-) */

	iph = ip_hdr(skb);
	ihl = iph->ihl * 4;

	if (x->encap) {
		struct xfrm_encap_tmpl *encap = x->encap;
		struct udphdr *uh = (void *)(skb_network_header(skb) + ihl);

		/*
		 * 1) if the NAT-T peer's IP or port changed then
		 *    advertize the change to the keying daemon.
		 *    This is an inbound SA, so just compare
		 *    SRC ports.
		 */
		if (iph->saddr != x->props.saddr.a4 ||
		    uh->source != encap->encap_sport) {
			xfrm_address_t ipaddr;

			ipaddr.a4 = iph->saddr;
			km_new_mapping(x, &ipaddr, uh->source);

			/* XXX: perhaps add an extra
			 * policy check here, to see
			 * if we should allow or
			 * reject a packet from a
			 * different source
			 * address/port.
			 */
		}

		/*
		 * 2) ignore UDP/TCP checksums in case
		 *    of NAT-T in Transport Mode, or
		 *    perform other post-processing fixes
		 *    as per draft-ietf-ipsec-udp-encaps-06,
		 *    section 3.1.2
		 */
		if (x->props.mode == XFRM_MODE_TRANSPORT)
			skb->ip_summed = CHECKSUM_UNNECESSARY;
	}

	pskb_trim(skb, skb->len - alen - padlen - 2);
	__skb_pull(skb, hlen);
	skb_set_transport_header(skb, -ihl);

	err = nexthdr[1];

	/* RFC4303: Drop dummy packets without any error */
	if (err == IPPROTO_NONE) {
		err = -EINVAL;
         }
	 
	 
out:
	return err;
}

/*
 * Note: detecting truncated vs. non-truncated authentication data is very
 * expensive, so we only support truncated data, which is the recommended
 * and common case.
 */
int cavium_ipsec_esp4_input(struct xfrm_state *x, struct sk_buff *skb, int offset)
{
	struct ip_esp_hdr *esph;
	struct crypto_aead *aead = x->data;
	struct sk_buff *trailer;
	int elen = skb->len - sizeof(*esph) - crypto_aead_ivsize(aead);
	int nfrags;
	int err = -EINVAL;
	int ret;
	
	skb_pull(skb, offset);

	if (!pskb_may_pull(skb, sizeof(*esph) + crypto_aead_ivsize(aead)))
		goto out;

	if (elen <= 0)
		goto out;

	if ((err = skb_cow_data(skb, 0, &trailer)) < 0)
		goto out;
	nfrags = err;

	err = -ENOMEM;

	skb->ip_summed = CHECKSUM_NONE;

	esph = (struct ip_esp_hdr *)skb->data;

	/* Get ivec. This can be wrong, check against another impls. */
        if(crypto_aead_ivsize(aead))
        memcpy(x->iv, esph->enc_data,crypto_aead_ivsize(aead));      
   {
      ret = cavium_process_esp_pkt(x->sa_handle, x->iv, skb);
      if (unlikely(ret)) {
         x->stats.integrity_failed++;
         goto out;
      }
	err = ret;
	err = esp4_input_done2(skb, err);
   }
      

out:
	return err;
}
static int esp6_input_done2(struct sk_buff *skb, int err)
{
	struct xfrm_state *x = xfrm_input_state(skb);
	struct crypto_aead *aead = x->data;
	int alen = crypto_aead_authsize(aead);
	int hlen = sizeof(struct ip_esp_hdr) + crypto_aead_ivsize(aead);
	int elen = skb->len - hlen;
	int hdr_len = skb_network_header_len(skb);
	u8 nexthdr[2];
	int padlen;


	if (unlikely(err))
		goto out;

	if (skb_copy_bits(skb, skb->len-alen-2, nexthdr, 2))
		BUG();

	err = -EINVAL;
	padlen = nexthdr[0];
	if (padlen + 2 + alen >= elen) 
		goto out;

	/* ... check padding bits here. Silly. :-) */

	pskb_trim(skb, skb->len - alen - padlen - 2);
	__skb_pull(skb, hlen);
	skb_set_transport_header(skb, -hdr_len);

	err = nexthdr[1];

	/* RFC4303: Drop dummy packets without any error */
	if (err == IPPROTO_NONE) {
		err = -EINVAL;
         }
	 
	 
out:
	return err;
}

/*
 * Note: detecting truncated vs. non-truncated authentication data is very
 * expensive, so we only support truncated data, which is the recommended
 * and common case.
 */
int cavium_ipsec_esp6_input(struct xfrm_state *x, struct sk_buff *skb, int offset)
{
	struct ip_esp_hdr *esph;
	struct crypto_aead *aead = x->data;
	struct sk_buff *trailer;
	int elen = skb->len - sizeof(*esph) - crypto_aead_ivsize(aead);
	int nfrags;
	int err = -EINVAL;
	int ret;
	
	skb_pull(skb, offset);

	if (!pskb_may_pull(skb, sizeof(*esph) + crypto_aead_ivsize(aead)))
		goto out;

	if (elen <= 0)
		goto out;

	if ((err = skb_cow_data(skb, 0, &trailer)) < 0)
		goto out;
	nfrags = err;

	err = -ENOMEM;

	skb->ip_summed = CHECKSUM_NONE;

	esph = (struct ip_esp_hdr *)skb->data;

	/* Get ivec. This can be wrong, check against another impls. */
        if(crypto_aead_ivsize(aead))
        memcpy(x->iv, esph->enc_data,crypto_aead_ivsize(aead));      
   {
      ret = cavium_process_esp_pkt(x->sa_handle, x->iv, skb);
      if (unlikely(ret)) {
         x->stats.integrity_failed++;
         goto out;
      }
	err = ret;
	err = esp6_input_done2(skb, err);
   }
      

out:
	return err;
}
/* Clear mutable options and find final destination to substitute
 * into IP header for icv calculation. Options are already checked
 * for validity, so paranoia is not required. 
 * This is from ah4.c [ip_clear_mutable_options()]
 */
static int cavium_ip4_clear_mutable_options(struct iphdr *iph, __be32 *daddr)
{
   unsigned char * optptr = (unsigned char*)(iph+1);
   int  l = iph->ihl*4 - sizeof(struct iphdr);
   int  optlen;

   while (l > 0) {
      switch (*optptr) {
         case IPOPT_END:
            return 0;
         case IPOPT_NOOP:
            l--;
            optptr++;
            continue;
      }
      optlen = optptr[1];
      if (optlen<2 || optlen>l)
         return -EINVAL;
      switch (*optptr) {
         case IPOPT_SEC:
         case 0x85:   /* Some "Extended Security" crap. */
         case IPOPT_CIPSO:
         case IPOPT_RA:
         case 0x80|21:   /* RFC1770 */
            break;
         case IPOPT_LSRR:
         case IPOPT_SSRR:
         if (optlen < 6)
            return -EINVAL;
         memcpy(daddr, optptr+optlen-4, 4);
         /* Fall through */
         default:
            memset(optptr+2, 0, optlen-2);
      }
      l -= optlen;
      optptr += optlen;
   }
   return 0;
}


int cavium_ipsec_ah4_output(struct xfrm_state *x, struct sk_buff *skb, int offset)
{
        int err;
        struct iphdr *iph, *top_iph;
        struct ip_auth_hdr *ah;
        struct ah_data *ahp;
        union {
                struct iphdr    iph;
                char            buf[60];
        } tmp_iph;

        skb_push(skb, -skb_network_offset(skb));
        top_iph = ip_hdr(skb);
        iph = &tmp_iph.iph;

        iph->tos = top_iph->tos;
        iph->ttl = top_iph->ttl;
        iph->frag_off = top_iph->frag_off;

        if (top_iph->ihl != 5) {
                iph->daddr = top_iph->daddr;
                memcpy(iph+1, top_iph+1, top_iph->ihl*4 - sizeof(struct iphdr));
                err = cavium_ip4_clear_mutable_options(top_iph, &top_iph->daddr);
                if (err)
                        goto error;
        }

        ah = ip_auth_hdr(skb);
        ah->nexthdr = *skb_mac_header(skb);
        *skb_mac_header(skb) = IPPROTO_AH;

        top_iph->tos = 0;
        top_iph->tot_len = htons(skb->len);
        top_iph->frag_off = 0;
        top_iph->ttl = 0;
        top_iph->check = 0;

        ahp = x->data;
        ah->hdrlen  = (XFRM_ALIGN8(sizeof(*ah) + ahp->icv_trunc_len) >> 2) - 2;

        ah->reserved = 0;
        ah->spi = x->id.spi;
        ah->seq_no = htonl(XFRM_SKB_CB(skb)->seq.output.low);

        spin_lock_bh(&x->lock);
        err = cavium_process_ah_pkt(x->sa_handle, ahp, skb, ah->auth_data);
        spin_unlock_bh(&x->lock);

        if (err)
                goto error;

        top_iph->tos = iph->tos;
        top_iph->ttl = iph->ttl;
        top_iph->frag_off = iph->frag_off;
        if (top_iph->ihl != 5) {
                top_iph->daddr = iph->daddr;
                memcpy(top_iph+1, iph+1, top_iph->ihl*4 - sizeof(struct iphdr));
        }

        err = 0;

error:
        return err;
}

int cavium_ipsec_ah4_input(struct xfrm_state *x, struct sk_buff *skb , int offset)
{
        int ah_hlen;
        int ihl;
        int nexthdr;
        int err = -EINVAL;
        struct iphdr *iph;
        struct ip_auth_hdr *ah;
        struct ah_data *ahp;
        char work_buf[60];

        skb_pull(skb, offset - offsetof(struct ip_auth_hdr, spi));

        if (!pskb_may_pull(skb, sizeof(*ah)))
                goto out;

        ah = (struct ip_auth_hdr *)skb->data;
        ahp = x->data;
        nexthdr = ah->nexthdr;
        ah_hlen = (ah->hdrlen + 2) << 2;

        if (ah_hlen != XFRM_ALIGN8(sizeof(*ah) + ahp->icv_full_len) &&
            ah_hlen != XFRM_ALIGN8(sizeof(*ah) + ahp->icv_trunc_len))
                goto out;

        if (!pskb_may_pull(skb, ah_hlen))
                goto out;

        /* We are going to _remove_ AH header to keep sockets happy,
 *          * so... Later this can change. */
        if (skb_cloned(skb) &&
            pskb_expand_head(skb, 0, 0, GFP_ATOMIC))
                goto out;

        skb->ip_summed = CHECKSUM_NONE;

        ah = (struct ip_auth_hdr *)skb->data;
        iph = ip_hdr(skb);
        ihl = skb->data - skb_network_header(skb);
        memcpy(work_buf, iph, ihl);

        iph->ttl = 0;
        iph->tos = 0;
        iph->frag_off = 0;
        iph->check = 0;
        if (ihl > sizeof(*iph)) {
                __be32 dummy;
                if (cavium_ip4_clear_mutable_options(iph, &dummy))
                        goto out;
        }

        spin_lock(&x->lock);
        {
                u8 auth_data[64]; //#define MAX_AH_AUTH_LEN 64 has been removed

                memcpy(auth_data, ah->auth_data, ahp->icv_trunc_len);
                skb_push(skb, ihl);
                err = cavium_process_ah_pkt(x->sa_handle, ahp, skb, ah->auth_data);
        }
        spin_unlock(&x->lock);
                if (err)
                        goto out;
        
	skb->network_header += ah_hlen;
        memcpy(skb_network_header(skb), work_buf, ihl);
        skb->transport_header = skb->network_header;
        __skb_pull(skb, ah_hlen + ihl);

        return nexthdr;

out:
        return err;
}

static int zero_out_mutable_opts(struct ipv6_opt_hdr *opthdr)
{
	u8 *opt = (u8 *)opthdr;
	int len = ipv6_optlen(opthdr);
	int off = 0;
	int optlen = 0;

	off += 2;
	len -= 2;

	while (len > 0) {

		switch (opt[off]) {

		case IPV6_TLV_PAD1:
			optlen = 1;
			break;
		default:
			if (len < 2)
				goto bad;
			optlen = opt[off+1]+2;
			if (len < optlen)
				goto bad;
			if (opt[off] & 0x20)
				memset(&opt[off+2], 0, opt[off+1]);
			break;
		}

		off += optlen;
		len -= optlen;
	}
	if (len == 0)
		return 1;

bad:
	return 0;
}
#if defined(CONFIG_IPV6_MIP6) || defined(CONFIG_IPV6_MIP6_MODULE)
/**
 *	ipv6_rearrange_destopt - rearrange IPv6 destination options header
 *	@iph: IPv6 header
 *	@destopt: destionation options header
 */
static void ipv6_rearrange_destopt(struct ipv6hdr *iph, struct ipv6_opt_hdr *destopt)
{
	u8 *opt = (u8 *)destopt;
	int len = ipv6_optlen(destopt);
	int off = 0;
	int optlen = 0;

	off += 2;
	len -= 2;

	while (len > 0) {

		switch (opt[off]) {

		case IPV6_TLV_PAD1:
			optlen = 1;
			break;
		default:
			if (len < 2)
				goto bad;
			optlen = opt[off+1]+2;
			if (len < optlen)
				goto bad;

			/* Rearrange the source address in @iph and the
			 * addresses in home address option for final source.
			 * See 11.3.2 of RFC 3775 for details.
			 */
			if (opt[off] == IPV6_TLV_HAO) {
				struct in6_addr final_addr;
				struct ipv6_destopt_hao *hao;

				hao = (struct ipv6_destopt_hao *)&opt[off];
				if (hao->length != sizeof(hao->addr)) {
					if (net_ratelimit())
//						CVM_LOG(KAME_IPSEC, INFO, "destopt hao: invalid header length: %u\n", hao->length);
					goto bad;
				}
				final_addr = hao->addr;
				hao->addr = iph->saddr;
				iph->saddr = final_addr;
			}
			break;
		}

		off += optlen;
		len -= optlen;
	}
	/* Note: ok if len == 0 */
bad:
	return;
}
#else
static void ipv6_rearrange_destopt(struct ipv6hdr *iph, struct ipv6_opt_hdr *destopt) {}
#endif
/**
 *  	ipv6_rearrange_rthdr - rearrange IPv6 routing header
 *   	@iph: IPv6 header
 *    	@rthdr: routing header
 *     
 *     	Rearrange the destination address in @iph and the addresses in @rthdr
 *      so that they appear in the order they will at the final destination.
 *     	See Appendix A2 of RFC 2402 for details.
 **/
static void ipv6_rearrange_rthdr(struct ipv6hdr *iph, struct ipv6_rt_hdr *rthdr)
{
	int segments, segments_left;
	struct in6_addr *addrs;
	struct in6_addr final_addr;

	segments_left = rthdr->segments_left;
	if (segments_left == 0)
		return;
	rthdr->segments_left = 0;

	/* The value of rthdr->hdrlen has been verified either by the system
  	 * call if it is locally generated, or by ipv6_rthdr_rcv() for incoming
  	 * packets.  So we can assume that it is even and that segments is
  	 * greater than or equal to segments_left.
  	 *
  	 * For the same reason we can assume that this option is of type 0. 
  	 */
	segments = rthdr->hdrlen >> 1;

	addrs = ((struct rt0_hdr *)rthdr)->addr;
	final_addr = addrs[segments - 1];

	addrs += segments - segments_left;
	memmove(addrs + 1, addrs, (segments_left - 1) * sizeof(*addrs));

	addrs[0] = iph->daddr;
	iph->daddr = final_addr;
}
static int cavium_ip6_clear_mutable_options(struct ipv6hdr *iph, int len, int dir)
{
        union {
                struct ipv6hdr *iph;
                struct ipv6_opt_hdr *opth;
                struct ipv6_rt_hdr *rth;
                char *raw;
        } exthdr = { .iph = iph };
        char *end = exthdr.raw + len;
        int nexthdr = iph->nexthdr;

        exthdr.iph++;

        while (exthdr.raw < end) {
                switch (nexthdr) {
                case NEXTHDR_DEST:
                        if (dir == XFRM_POLICY_OUT)
                                ipv6_rearrange_destopt(iph, exthdr.opth);
                case NEXTHDR_HOP:
                        if (!zero_out_mutable_opts(exthdr.opth)) {
                                printk(KERN_INFO"overrun %sopts\n",
                                        nexthdr == NEXTHDR_HOP ?
                                                "hop" : "dest");
                                return -EINVAL;
                        }
                        break;

                case NEXTHDR_ROUTING:
                        ipv6_rearrange_rthdr(iph, exthdr.rth);
                        break;

                default :
                        return 0;
                }
                nexthdr = exthdr.opth->nexthdr;
                exthdr.raw += ipv6_optlen(exthdr.opth);
        }

        return 0;
}



static int cavium_ipsec_ah6_output(struct xfrm_state *x, struct sk_buff *skb,
                                int offset)
{
        int err;
        int extlen;
        struct ipv6hdr *top_iph;
        struct ip_auth_hdr *ah;
        struct ah_data *ahp;
        u8 nexthdr;
        char tmp_base[8];
        struct {
#if defined(CONFIG_IPV6_MIP6) || defined(CONFIG_IPV6_MIP6_MODULE)
                struct in6_addr saddr;
#endif
                struct in6_addr daddr;
                char hdrs[0];
        } *tmp_ext;

        skb_push(skb, -skb_network_offset(skb));
        top_iph = ipv6_hdr(skb);
        top_iph->payload_len = htons(skb->len - sizeof(*top_iph));

        nexthdr = *skb_mac_header(skb);
        *skb_mac_header(skb) = IPPROTO_AH;

        /* When there are no extension headers, we only need to save the first
         * 8 bytes of the base IP header.
         */
        memcpy(tmp_base, top_iph, sizeof(tmp_base));

        tmp_ext = NULL;
        extlen = skb_transport_offset(skb) - sizeof(struct ipv6hdr);
        if (extlen) {
                extlen += sizeof(*tmp_ext);
                tmp_ext = kmalloc(extlen, GFP_ATOMIC);
                if (!tmp_ext) {
                        err = -ENOMEM;
                        goto error;
                }
#if defined(CONFIG_IPV6_MIP6) || defined(CONFIG_IPV6_MIP6_MODULE)
                memcpy(tmp_ext, &top_iph->saddr, extlen);
#else
                memcpy(tmp_ext, &top_iph->daddr, extlen);
#endif
                err = cavium_ip6_clear_mutable_options(top_iph,
                                                 extlen - sizeof(*tmp_ext) +
                                                 sizeof(*top_iph),
                                                 XFRM_POLICY_OUT);
                if (err)
                        goto error_free_iph;
        }

        ah = ip_auth_hdr(skb);
        ah->nexthdr = nexthdr;

        top_iph->priority    = 0;
        top_iph->flow_lbl[0] = 0;
        top_iph->flow_lbl[1] = 0;
        top_iph->flow_lbl[2] = 0;
        top_iph->hop_limit   = 0;

        ahp = x->data;
        ah->hdrlen  = (XFRM_ALIGN8(sizeof(*ah) + ahp->icv_trunc_len) >> 2) - 2;

        ah->reserved = 0;
        ah->spi = x->id.spi;
        ah->seq_no = htonl(XFRM_SKB_CB(skb)->seq.output.low);

        spin_lock_bh(&x->lock);
        err = cavium_process_ah_pkt(x->sa_handle, ahp, skb, ah->auth_data);
        spin_unlock_bh(&x->lock);
  if (err)
                goto error_free_iph;

        memcpy(top_iph, tmp_base, sizeof(tmp_base));
        if (tmp_ext) {
#if defined(CONFIG_IPV6_MIP6) || defined(CONFIG_IPV6_MIP6_MODULE)
                memcpy(&top_iph->saddr, tmp_ext, extlen);
#else
                memcpy(&top_iph->daddr, tmp_ext, extlen);
#endif
error_free_iph:
                kfree(tmp_ext);
        }

error:
        return err;
}


static int cavium_ipsec_ah6_input(struct xfrm_state *x, struct sk_buff *skb, int offset)
{
        /*
         * Before process AH
         * [IPv6][Ext1][Ext2][AH][Dest][Payload]
         * |<-------------->| hdr_len
         *
         * To erase AH:
         * Keeping copy of cleared headers. After AH processing,
         * Moving the pointer of skb->network_header by using skb_pull as long
         * as AH header length. Then copy back the copy as long as hdr_len
         * If destination header following AH exists, copy it into after [Ext2].
         *
         * |<>|[IPv6][Ext1][Ext2][Dest][Payload]
         * There is offset of AH before IPv6 header after the process.
         */

        struct ip_auth_hdr *ah;
        struct ipv6hdr *ip6h;
        struct ah_data *ahp;
        unsigned char *tmp_hdr = NULL;
        u16 hdr_len;
        u16 ah_hlen;
        int nexthdr;
        int err = -EINVAL;

	skb_pull(skb, offset - offsetof(struct ip_auth_hdr, spi));

        if (!pskb_may_pull(skb, sizeof(struct ip_auth_hdr)))
                goto out;

        /* We are going to _remove_ AH header to keep sockets happy,
         * so... Later this can change. */
        if (skb_cloned(skb) &&
            pskb_expand_head(skb, 0, 0, GFP_ATOMIC))
           goto out;

        skb->ip_summed = CHECKSUM_NONE;

        hdr_len = skb->data - skb_network_header(skb);
        ah = (struct ip_auth_hdr *)skb->data;
        ahp = x->data;
        nexthdr = ah->nexthdr;
        ah_hlen = (ah->hdrlen + 2) << 2;

        if (ah_hlen != XFRM_ALIGN8(sizeof(*ah) + ahp->icv_full_len) &&
            ah_hlen != XFRM_ALIGN8(sizeof(*ah) + ahp->icv_trunc_len))
                goto out;

        if (!pskb_may_pull(skb, ah_hlen))
                goto out;

        tmp_hdr = kmemdup(skb_network_header(skb), hdr_len, GFP_ATOMIC);
        if (!tmp_hdr)
                goto out;
        ip6h = ipv6_hdr(skb);
        if (cavium_ip6_clear_mutable_options(ip6h, hdr_len, XFRM_POLICY_IN))
                goto free_out;
        ip6h->priority    = 0;
        ip6h->flow_lbl[0] = 0;
        ip6h->flow_lbl[1] = 0;
        ip6h->flow_lbl[2] = 0;
        ip6h->hop_limit   = 0;

        spin_lock(&x->lock);
        {
                u8 auth_data[64]; //#define MAX_AH_AUTH_LEN 64 has been removed

                memcpy(auth_data, ah->auth_data, ahp->icv_trunc_len);
                memset(ah->auth_data, 0, ahp->icv_trunc_len);
                skb_push(skb, hdr_len);
                err = cavium_process_ah_pkt(x->sa_handle, ahp, skb, ah->auth_data);	
        }
        spin_unlock(&x->lock);

        if (err)
                goto free_out;

        skb->network_header += ah_hlen;
        memcpy(skb_network_header(skb), tmp_hdr, hdr_len);
        skb->transport_header = skb->network_header;
        __skb_pull(skb, ah_hlen + hdr_len);

        kfree(tmp_hdr);

        return nexthdr;

free_out:
        kfree(tmp_hdr);
out:
        return err;
}


static cavium_process_fn get_ah_ipsec_fn_name(struct xfrm_state *x)
{
   cavium_process_fn ret = NULL;
   int dir = 0;
#if defined(CONFIG_IPV6) || defined (CONFIG_IPV6_MODULE)
   if (x->props.family == AF_INET6) {
      dir = (ipv6_chk_addr(&init_net ,((struct in6_addr *) (&x->id.daddr)), NULL, 0) ? 1 : 0);
    } else
#endif
   if (x->props.family == AF_INET) {
      dir = ((inet_addr_type(&init_net,x->id.daddr.a4) == RTN_LOCAL) ? 1 : 0);
      /* Inbound 1, outbound 0 */
   } else {
      printk("%s: Invalid x->props.family %d \n", __FUNCTION__,
             x->props.family);
      return ret;
   }
   if (dir)
      if(x->props.family == AF_INET6)
        ret = cavium_ipsec_ah6_input;
      else
        ret = cavium_ipsec_ah4_input;
   else
      if(x->props.family == AF_INET6)
        ret = cavium_ipsec_ah6_output;
       else
        ret = cavium_ipsec_ah4_output;

   return ret;

}

static cavium_process_fn get_esp_ipsec_fn_name(struct xfrm_state *x)
{
   cavium_process_fn ret = NULL;
   int dir = 0;
#if defined(CONFIG_IPV6) || defined (CONFIG_IPV6_MODULE)
   if (x->props.family == AF_INET6) {
      dir = (ipv6_chk_addr(&init_net ,((struct in6_addr *) (&x->id.daddr)), NULL, 0) ? 1 : 0);
   } else
#endif
   if (x->props.family == AF_INET) {
      dir = ((inet_addr_type(&init_net,x->id.daddr.a4) == RTN_LOCAL) ? 1 : 0);
   	/* Inbound 1, outbound 0 */
   } else {
      printk("%s: Invalid x->props.family %d \n", __FUNCTION__,
             x->props.family);
      return ret;
   }
   if (dir)
      if(x->props.family == AF_INET6)
        ret = cavium_ipsec_esp6_input;
      else
        ret = cavium_ipsec_esp4_input;
   else
      if(x->props.family == AF_INET6)
        ret = cavium_ipsec_esp6_output;
       else
        ret = cavium_ipsec_esp4_output;


   return ret;
}


static cavium_ipsec_fn get_ah_fn_name(struct xfrm_state *x)
{
   int aalg = x->props.aalgo;
   cavium_ipsec_fn ret = NULL;

   /* By default assume sha1 APIs */
   if (aalg == SADB_AALG_SHA1HMAC) {
   ret = AH_sha1;
   } else if (aalg == SADB_AALG_MD5HMAC) {
      ret = AH_md5;
   } else {
      ret = NULL;
   }

   return ret;
}

static cavium_ipsec_fn get_esp_fn_name(struct xfrm_state *x)
{
   int aalg = x->props.aalgo;
   int ealg = x->props.ealgo;
   cavium_ipsec_fn ret = NULL;
   int dir = 0;

#if defined(CONFIG_IPV6) || defined (CONFIG_IPV6_MODULE)
   if (x->props.family == AF_INET6) {
      dir = (ipv6_chk_addr(&init_net ,((struct in6_addr *) (&x->id.daddr)), NULL, 0) ? 1 : 0);
   } else
#endif
   if (x->props.family == AF_INET) {
      dir = ((inet_addr_type(&init_net,x->id.daddr.a4) == RTN_LOCAL) ? 1 : 0);
      /* Inbound 1, outbound 0 */
   } else {
      printk("%s: Invalid x->props.family %d \n", __FUNCTION__,
              x->props.family);
      return ret;
   }

   /* By default assume sha1 APIs */
   switch (ealg) {
      case SADB_EALG_DESCBC:
      case SADB_EALG_3DESCBC:
         if (dir) {
            ret = DES_CBC_sha1_decrypt;
         } else {
            ret = DES_CBC_sha1_encrypt;
         }
         break;
      case SADB_X_EALG_AESCBC:
         if (dir) {
            ret = AES_CBC_sha1_decrypt;
         } else {
            ret = AES_CBC_sha1_encrypt;
         }
         break;
      case SADB_EALG_NULL:
         if (dir) {
            ret = NULL_sha1_decrypt;
         } else {
            ret = NULL_sha1_encrypt;
         }
         break;
      default:
         return ret;
   }

   if (aalg == SADB_AALG_SHA1HMAC)
      return ret;

    // The below is a temporary fix for SHA256, SHA384 and SHA512 to use kernel path.
    // Proper changes are to be added.
    if ((aalg == SADB_X_AALG_SHA2_256HMAC) || (aalg == SADB_X_AALG_SHA2_384HMAC) || (aalg == SADB_X_AALG_SHA2_512HMAC))
    {
        ret = NULL;
        return ret;
    }

   if (aalg != SADB_AALG_MD5HMAC) {
      if (aalg == SADB_AALG_NONE) {
         switch(ealg) { 
         case SADB_EALG_DESCBC:
         case SADB_EALG_3DESCBC:
            if (dir) {
               ret = DES_CBC_decrypt;
            } else {
               ret = DES_CBC_encrypt;
            }
         break;
         case SADB_X_EALG_AESCBC:
            if (dir) {
               ret = AES_CBC_decrypt;
            } else {
               ret = AES_CBC_encrypt;
            }
         break;
         default:
            ret = NULL;
         }
      }

      return ret;
   }

   switch (ealg) {
      case SADB_EALG_DESCBC:
      case SADB_EALG_3DESCBC:
         if (dir) {
            ret = DES_CBC_md5_decrypt;
         } else {
            ret = DES_CBC_md5_encrypt;
         }
         break;
      case SADB_X_EALG_AESCBC:
         if (dir) {
            ret = AES_CBC_md5_decrypt;
         } else {
            ret = AES_CBC_md5_encrypt;
         }
         break;
      case SADB_EALG_NULL:
         if (dir) {
            ret = NULL_md5_decrypt;
         } else {
            ret = NULL_md5_encrypt;
         }
         break;
      default:
   printk("%s: Should never happen .....\n", __FUNCTION__);
   ret = NULL;
   }

   return ret;
}

void cavium_delete_hndl(struct xfrm_state *x)
{
   if (unlikely(x && x->sa_handle)) {
      CAV_DEL_FROM_GLOB_LIST(x->sa_handle);
      kfree(x->sa_handle);
      x->sa_handle = NULL;
   } else {
      pr_debug("%s : NULL Sa/SA Handle : with x %p x->sa_handle %p\n", 
                __FUNCTION__, x ? x: NULL, x->sa_handle ? x->sa_handle: NULL);
   }
   return;
}

void *
cavium_alloc_n_fill(struct xfrm_state *x)
{
   oct_data_t *buf;
   cavium_ipsec_fn fn = NULL;
   cavium_process_fn ipsec_fn = NULL;
   struct esp_data *esp = NULL;
   struct ah_data *ah = NULL;
   uint64_t *key = NULL;

   if (x->type == NULL) {
      printk("%s: xfrm->type is not filled. \n", __FUNCTION__);
      return NULL;
   }

   if (x->type->proto == IPPROTO_ESP) {
      fn = get_esp_fn_name(x);
      ipsec_fn = get_esp_ipsec_fn_name(x);
      esp = (struct esp_data *) (x->data);
      if (x->aalg) 
         key = (uint64_t *) (x->aalg->alg_key);
   } else if (x->type->proto == IPPROTO_AH) {
      fn = get_ah_fn_name(x);
      ipsec_fn = get_ah_ipsec_fn_name(x);
      ah = (struct ah_data *) (x->data);
      if (x->aalg) 
         key = (uint64_t *) (x->aalg->alg_key);
      } else {
         printk("%s: Protocol type not valid. \n", __FUNCTION__);
         return NULL;
   }

   if (fn == NULL) {
      pr_debug("%s: Cipher/Digest not supported. \n", __FUNCTION__);
      return NULL;
   }

   if (ipsec_fn == NULL) {
      printk("%s: Invalid Protocol Family \n", __FUNCTION__);
      return NULL;
   }

   buf = (oct_data_t *) kmalloc(sizeof(oct_data_t), GFP_KERNEL);
   if (buf == NULL) {
      printk("%s: Unable to allocate memory for cavium ipsec offload\n"
             "         So, this SA will not be functional..\n", __FUNCTION__);
      return buf;
   }

   memset(buf, 0, sizeof(*buf));

   if (key)
      cav_calc_hash(((x->props.aalgo == SADB_AALG_SHA1HMAC) ? 1 : 0),
                     key, buf->inner_hash, buf->outer_hash);

   buf->oct_fn = fn;
   buf->process_fn = ipsec_fn;
   buf->xfrm = x;
   CAV_INSERT_IN_GLOB_LIST(buf);
   return (void *) buf;
}

