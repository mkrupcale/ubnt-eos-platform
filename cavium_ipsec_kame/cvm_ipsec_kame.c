/***************************************************************************
 * Copyright (c) 2003 - 2012 Cavium Networks (support@cavium.com). All rights
 * reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *
 *     * Neither the name of Cavium Networks nor the names of
 *       its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written
 *       permission.
 *
 * This Software, including technical data, may be subject to U.S. export
 * control laws, including the U.S. Export Administration Act and its
 * associated regulations, and may be subject to export or import regulations
 * in other countries. You warrant that You will comply strictly in all
 * respects with all such regulations and acknowledge that you have the
 * responsibility to obtain licenses to export, re-export or import the
 * Software.
 * 
 * TO THE MAXIMUM EXTENT PERMITTED BY LAW, THE SOFTWARE IS PROVIDED "AS IS"
 * AND WITH ALL FAULTS AND CAVIUM NETWORKS MAKES NO PROMISES, REPRESENTATIONS
 * OR WARRANTIES, EITHER EXPRESS, IMPLIED, STATUTORY, OR OTHERWISE, WITH
 * RESPECT TO THE SOFTWARE, INCLUDING ITS CONDITION, ITS CONFORMITY TO ANY
 * REPRESENTATION OR DESCRIPTION, OR THE EXISTENCE OF ANY LATENT OR PATENT
 * DEFECTS, AND CAVIUM SPECIFICALLY DISCLAIMS ALL IMPLIED (IF ANY) WARRANTIES
 * OF TITLE, MERCHANTABILITY, NONINFRINGEMENT, FITNESS FOR A PARTICULAR
 * PURPOSE, LACK OF VIRUSES, ACCURACY OR COMPLETENESS, QUIET ENJOYMENT, QUIET
 * POSSESSION OR CORRESPONDENCE TO DESCRIPTION.  THE ENTIRE RISK ARISING OUT
 * OF USE OR PERFORMANCE OF THE SOFTWARE LIES WITH YOU.
 *
 ****************************************************************************/

#include<linux/kernel.h>
#include<linux/module.h>
#include <linux/skbuff.h>
#include <net/xfrm.h>
#include <linux/string.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <net/ip.h>
#include "cavium_ipsec.h"


extern void set_cavium_ipsec_process(void *);
spinlock_t cav_sahdl_lock;
struct list_head cav_sahdl_list;

int ipsec_process(void *data, struct sk_buff *skb, int offset, int enc)
{
	struct xfrm_state *x = (struct xfrm_state *) data;
	if (unlikely(x->sa_handle == NULL)) {
		printk("%s: x->sa_handle is NULL \n", __FUNCTION__);
		return -1;
	}

	return (((oct_data_t *) x->sa_handle)->process_fn(x, skb, offset));
}

static void
cavium_flush_all(void)
{
	struct list_head *ent, *temp;

	list_for_each_safe(ent, temp, &cav_sahdl_list) {
		oct_data_t *d = list_entry(ent, oct_data_t, list);
		cavium_delete_hndl(d->xfrm);
	}
	return;
}

static int
cvm_xfrm_notify(struct xfrm_state *x, const struct km_event *c)
{
   switch (c->event) {
		case XFRM_MSG_UPDSA:
		   cavium_delete_hndl(x);
      case XFRM_MSG_NEWSA:
         x->sa_handle = cavium_alloc_n_fill(x);
         break;
		case XFRM_MSG_EXPIRE:
		   if (!c->data.hard)
				break;
		case XFRM_MSG_DELSA:
		   cavium_delete_hndl(x);
		   break;
      case XFRM_MSG_FLUSHSA:
         cavium_flush_all();
         break;
		default:
			printk("Event on unsupported default hdlr %d \n", c->event);
   }
   return 0;
}

static int
cvm_xfrm_unused_acq(struct xfrm_state *x, struct xfrm_tmpl *t,
                    struct xfrm_policy *xp)
{
   return -1;
}

static struct xfrm_policy *
cvm_xfrm_unused_comp(struct sock *sk, int opt, u8 *d,
                     int len, int *dir)
{
   *dir = -1;
   return NULL;
}

struct xfrm_mgr cvm_xfrm_mgr = 
{
   .id = "cavium",
   .notify = cvm_xfrm_notify,
   .acquire = cvm_xfrm_unused_acq,
   .compile_policy = cvm_xfrm_unused_comp,
} ;


static int __init ipsec_init(void)
{
	set_cavium_ipsec_process((void *) ipsec_process);
	xfrm_register_km(&cvm_xfrm_mgr);
	spin_lock_init(&cav_sahdl_lock);
	INIT_LIST_HEAD(&cav_sahdl_list);
	return 0;

}
static void __exit ipsec_exit(void)
{
	cavium_flush_all();
	set_cavium_ipsec_process(NULL);
	xfrm_unregister_km(&cvm_xfrm_mgr);
	return;
}


//EXPORT_SYMBOL(set_cavium_ipsec_proces);
module_init(ipsec_init);
module_exit(ipsec_exit);
MODULE_LICENSE("GPL");
