/**********************************************************************
* Author: Cavium, Inc.
*
* Contact: support@cavium.com
*          Please include "ipfwd-offload" in the subject.
*
* Copyright (c) 2003-2016 Cavium, Inc.
*
* This file is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License, Version 2, as
* published by the Free Software Foundation.
*
* This file is distributed in the hope that it will be useful, but
* AS-IS and WITHOUT ANY WARRANTY; without even the implied warranty
* of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE, TITLE, or
* NONINFRINGEMENT.  See the GNU General Public License for more details.
***********************************************************************/

#include <linux/cache.h>
#include <asm/barrier.h>
#include <linux/crc32.h>

#include "platform_api.h"

int platform_get_hash_bucket(struct sk_buff *skb, void *iph, struct tcphdr *th, 
								struct cvm_packet_info *cvm_info, bool ip6)
{
	u64 sdaddr;
	u32 saddr, daddr, out_saddr, out_daddr;
	u32 sdport;
	u16 out_sport, out_dport;
	u8 proto, out_proto;
	u32 gre_key;
	int idx;

	set_c0_status(ST0_CU2);

	CVMX_MT_CRC_POLYNOMIAL (0x1edc6f41);
	CVMX_MT_CRC_IV (0);

	if (ip6) {
		struct ipv6hdr *ip;
		ip = (struct ipv6hdr *)iph;
		sdaddr = *(u64 *)&ip->saddr;
		CVMX_MT_CRC_DWORD (sdaddr);

		sdaddr = *((u64 *)&ip->saddr + 1);
		CVMX_MT_CRC_DWORD (sdaddr);

		proto = ip->nexthdr;
		CVMX_MT_CRC_BYTE (proto);
	} else {
		struct iphdr *ip;
		ip = (struct iphdr *)iph;
		saddr = ip->saddr;
		CVMX_MT_CRC_WORD (saddr);

		daddr = ip->daddr;
		CVMX_MT_CRC_WORD (daddr);

		proto = ip->protocol;
		CVMX_MT_CRC_BYTE (proto);
	}

	sdport = *(u32*)&th->source;
	CVMX_MT_CRC_WORD (sdport);

	if (cvm_info->rx_pkt_flags) {
		if (test_vlan_hdr(&cvm_info->rx_pkt_flags)) {
			int vlan_tci = cvm_info->vlan.h_vlan_TCI;	
			CVMX_MT_CRC_WORD(vlan_tci);	
		}
		if (test_pppoe_hdr(&cvm_info->rx_pkt_flags)) {
			int pppoe_sid = cvm_info->pppoe.sid;
			CVMX_MT_CRC_WORD(pppoe_sid);
		}
		if (!ip6) {
			if (test_vxlan_hdr(&cvm_info->rx_pkt_flags)) {
				int vx_vni = cvm_info->vxlan.vx_vni;
				CVMX_MT_CRC_WORD(vx_vni);

				out_saddr = cvm_info->outer_ip4.saddr;	
				CVMX_MT_CRC_WORD(out_saddr);
				out_daddr = cvm_info->outer_ip4.daddr;
				CVMX_MT_CRC_WORD(out_daddr);
				out_proto = cvm_info->outer_ip4.protocol;
				CVMX_MT_CRC_BYTE(out_proto);
				out_sport = cvm_info->outer_udp.source;
				CVMX_MT_CRC_WORD(out_sport);
				out_dport = cvm_info->outer_udp.dest;
				CVMX_MT_CRC_WORD(out_dport);
			}
		}
		if(test_gre_hdr(&cvm_info->rx_pkt_flags)){
			
			out_saddr = cvm_info->outer_ip4.saddr;	
			CVMX_MT_CRC_WORD(out_saddr);
			out_daddr = cvm_info->outer_ip4.daddr;
			CVMX_MT_CRC_WORD(out_daddr);
			out_proto = cvm_info->outer_ip4.protocol;
			CVMX_MT_CRC_BYTE(out_proto);
			if(cvm_info->gre.flags & GRE_KEY){
				gre_key = cvm_info->gre.key;
				CVMX_MT_CRC_BYTE(gre_key);
			}
		}		
	
	}

	CVMX_MF_CRC_IV (idx);

	if (ip6)
		return (idx & CVM_IPV6FWD_LUT_MASK);
	else
		return (idx & CVM_IPFWD_LUT_MASK);
}

int platform_get_hash_filter_bucket(filter_keys_t *fkeys)
{
	int idx;
	u8 proto = 0;
	u16 sport = 0, dport = 0;
	u32 mark = 0;

	set_c0_status(ST0_CU2);
	
	if (fkeys->flags & IPFWD_FILTER_PROTO)
		proto = fkeys->proto;
	if (fkeys->flags & IPFWD_FILTER_SPORT)
		sport = fkeys->sport;
	if (fkeys->flags & IPFWD_FILTER_DPORT)
		dport = fkeys->dport;
	if (fkeys->flags & IPFWD_FILTER_MARK)
		mark = fkeys->mark;

	CVMX_MT_CRC_POLYNOMIAL(0x1edc6f41);
	CVMX_MT_CRC_IV(0);
	CVMX_MT_CRC_BYTE(proto);
	CVMX_MT_CRC_WORD(sport);
	CVMX_MT_CRC_WORD(dport);
	CVMX_MT_CRC_DWORD(mark);

	CVMX_MF_CRC_IV(idx);

	return (idx & CVM_IPFWD_FILTER_LUT_MASK);
}

int platform_get_hash_frag_bucket(struct iphdr *ip) 
{
    uint64_t sdaddr;
    uint32_t frag_id;
    uint8_t proto;
    int idx;

    set_c0_status(ST0_CU2);

    proto=ip->protocol;

    CVMX_MT_CRC_POLYNOMIAL (0x1edc6f41);
    CVMX_MT_CRC_IV (0);
    sdaddr = *(uint64_t*)&ip->saddr;
    CVMX_MT_CRC_DWORD (sdaddr);
    frag_id = ip->id;
    CVMX_MT_CRC_WORD (frag_id);
    CVMX_MT_CRC_BYTE (proto);
    CVMX_MF_CRC_IV (idx);

    return (idx & CVM_IPFWD_LUT_MASK);
}

