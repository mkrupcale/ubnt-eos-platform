#ifndef UBNT_NF_APP_DEBUG_H_
#define UBNT_NF_APP_DEBUG_H_


#define FWMOD_NAME "nf_dpi"

#define ERR(fmt, args...) printk(KERN_ERR " *** ERROR: [%s:%d] " fmt "\n", __func__, __LINE__, ##args)
#define PRT(fmt, args...) printk(KERN_INFO FWMOD_NAME ": " fmt "\n", ##args)

////////////////////////////////////////////////////////////////////////////////
#define MAC_OCTET_FMT "%02X:%02X:%02X:%02X:%02X:%02X"
#define MAC_OCTET_EXPAND(o) (uint8_t) o[0], (uint8_t) o[1], (uint8_t) o[2], (uint8_t) o[3], (uint8_t) o[4], (uint8_t) o[5]
#define IPV4_OCTET_FMT "%u.%u.%u.%u"
#define IPV4_OCTET_EXPAND(o) (uint8_t) o[0], (uint8_t) o[1], (uint8_t) o[2], (uint8_t) o[3]

#define IPV6_OCTET_FMT "%02X%02X:%02X%02X:%02X%02X:%02X%02X:%02X%02X:%02X%02X:%02X%02X:%02X%02X"
#define IPV6_OCTET_EXPAND(o) \
	(uint8_t) o[0], (uint8_t) o[1], (uint8_t) o[2], (uint8_t) o[3], \
	(uint8_t) o[4], (uint8_t) o[5], (uint8_t) o[6], (uint8_t) o[7], \
	(uint8_t) o[8], (uint8_t) o[9], (uint8_t) o[10], (uint8_t) o[11], \
	(uint8_t) o[12], (uint8_t) o[13], (uint8_t) o[14], (uint8_t) o[15]


////////////////////////////////////////////////////////////////////////////////
#include "skb_access.h"

static inline __attribute__((unused)) void ipv6_to_str(uint8_t *ipv6_octet, int ipv6_octet_len, char *buf, int buf_len)
{
	register int i = 0;
	register int buf_used_len = 0;

	if (ipv6_octet_len != 16)
	{
		ERR("Invalid IPv6 address length %d\n", ipv6_octet_len);
		return;
	}

	for (i = 0; i < (16 / 2); i++)
	{
		buf_used_len += snprintf(buf + buf_used_len, buf_len - buf_used_len,
				"%02X%02X%s",
				ipv6_octet[i * 2], ipv6_octet[(i * 2) + 1], (i == (16 / 2) - 1) ? "" : ":");
	}
}

static inline __attribute__((unused)) void ipv4_to_str(uint8_t *ipv4_octet, int ipv4_octet_len, char *buf, int buf_len)
{
	if (ipv4_octet_len != 4)
	{
		ERR("Invalid IPv4 address length %d\n", ipv4_octet_len);
		return;
	}

	snprintf(buf, buf_len, IPV4_OCTET_FMT, IPV4_OCTET_EXPAND(ipv4_octet));
}

static inline __attribute__((unused)) void skb_debug_ipv6(struct sk_buff *skb)
{
	static unsigned pkt_cnt = 0;
	char *proto = "";
	char unknown[8];

	char srcstr[128];
	char dststr[128];

	if (SKB_IPV6_HEAD_ADDR(skb) == NULL)
	{
		ERR("Invalid IPv6 header address");
		return;
	}

	switch (SKB_IPV6_NHDR(skb))
	{
	case IPPROTO_TCP:
		proto = "TCP";
		break;
	case IPPROTO_UDP:
		proto = "UDP";
		break;
	case IPPROTO_ICMP:
		proto = "ICMP";
		break;
	case IPPROTO_IGMP:
		proto = "IGMP";
		break;
	case IPPROTO_IPV6:
		proto = "IPv6";
		break;
	default:
		proto = unknown;
		snprintf(unknown, sizeof(unknown), "%d", SKB_IP_PRO(skb));
		break;
	}

	ipv6_to_str((uint8_t *) &SKB_IPV6_IN6SIP(skb), 16, srcstr, sizeof(srcstr));
	ipv6_to_str((uint8_t *) &SKB_IPV6_IN6DIP(skb), 16, dststr, sizeof(dststr));

	if (SKB_ETH(skb))
	{
		printk(KERN_DEBUG " + [%u] %s, %s -> %s len %u mark x%x smac=" MAC_OCTET_FMT " (%s) dmac=" MAC_OCTET_FMT "\n",
				pkt_cnt, proto, srcstr, dststr, skb->len, skb->mark,
				MAC_OCTET_EXPAND(SKB_ETH_SRC(skb)),
				skb->dev->name,
				MAC_OCTET_EXPAND(SKB_ETH_DST(skb)));
	}
	else
	{
		printk(KERN_DEBUG " + [%u] %s, %s -> %s len %u mark x%x\n",
				pkt_cnt, proto, srcstr, dststr, skb->len, skb->mark);
	}

	pkt_cnt++;
}

static inline __attribute__((unused)) void skb_debug(struct sk_buff *skb)
{
	char *proto = "";
	char unknown[8];

#if CONFIG_IPV6_HOOK
	if (SKB_ETH_PRO(skb) == htons(ETH_P_IPV6))
	{
		skb_debug_ipv6(skb);
		return;
	}
#endif

	/*
	 * Dump packet msg
	 */

	switch (SKB_IP_PRO(skb))
	{
	case IPPROTO_TCP:
		proto = "TCP";
		break;
	case IPPROTO_UDP:
		proto = "UDP";
		break;
	case IPPROTO_ICMP:
		proto = "ICMP";
		break;
	case IPPROTO_IGMP:
		proto = "IGMP";
		break;
	case IPPROTO_IPV6:
		proto = "IPv6";
		break;
	default:
		proto = unknown;
		snprintf(unknown, sizeof(unknown), "%d", SKB_IP_PRO(skb));
		break;
	}

	{
		char srcstr[16] = { 0x00 };
		char dststr[16] = { 0x00 };

		ipv4_to_str((uint8_t *) &SKB_IP_SIP(skb), 4, srcstr, sizeof(srcstr));
		ipv4_to_str((uint8_t *) &SKB_IP_DIP(skb), 4, dststr, sizeof(dststr));

		if (SKB_ETH(skb))
		{
			printk(KERN_DEBUG " * %s, %s -> %s (skb=%u, pkt=%d, hlen=%d) mark=0x%x smac=" MAC_OCTET_FMT " (%s) dmac=" MAC_OCTET_FMT "\n",
				proto,
				srcstr,
				dststr,
				skb->len,
				ntohs(SKB_IP_TOT_LEN(skb)),
				SKB_IP_IHL(skb) * 4,
				skb->mark,
				MAC_OCTET_EXPAND(SKB_ETH_SRC(skb)),
				skb->dev->name,
				MAC_OCTET_EXPAND(SKB_ETH_DST(skb)));
		}
		else
		{
			printk(KERN_DEBUG " * %s, %s -> %s (skb=%u, pkt=%d, hlen=%d) mark=0x%x\n",
				proto,
				srcstr,
				dststr,
				skb->len,
				ntohs(SKB_IP_TOT_LEN(skb)),
				SKB_IP_IHL(skb) * 4,
				skb->mark);
		}
	}
}

static inline void action_handler(tdts_pkt_parameter_t *pkt_param)
{
	if(tdts_check_pkt_parameter_res(pkt_param, TDTS_RES_TYPE_APPID))
	{
		if (TDTS_PKT_PARAMETER_RES_APPID_CHECK_NOINT(pkt_param) /* No interest. */)
		{
			/*
			 * TDTS is not interested in this packet or TCP/UDP flow.
			 * It's safe to bypass if IPS is not turned on.
			 */
			PRT("APPID -> Cat=0, App=0, Beh=0 (No interest)");
		}
		else
		{
			/*
			 * Mmm, here're the results!
			 *
			 * TDTS_PKT_PARAMETER_RES_APPID_CHECK_FINAL: TDTS makes sure the cat & app of this flow.
			 * TDTS_PKT_PARAMETER_RES_APPID_CHECK_NOMORE: It's safe to bypass APPID for this flow!
			 */
			PRT("APPID -> Cat=%u(%s), App=%u(%s), Beh=%u(%s)%s%s",
				TDTS_PKT_PARAMETER_RES_APPID_CAT_ID(pkt_param),
				TDTS_PKT_PARAMETER_RES_APPID_CAT_NAME(pkt_param),
				TDTS_PKT_PARAMETER_RES_APPID_APP_ID(pkt_param),
				TDTS_PKT_PARAMETER_RES_APPID_APP_NAME(pkt_param),
				TDTS_PKT_PARAMETER_RES_APPID_BEH_ID(pkt_param),
				TDTS_PKT_PARAMETER_RES_APPID_BEH_NAME(pkt_param),
				TDTS_PKT_PARAMETER_RES_APPID_CHECK_FINAL(pkt_param) ? ", Final" : "",
				TDTS_PKT_PARAMETER_RES_APPID_CHECK_NOMORE(pkt_param) ? ", Nomore" : "");
		}
	}
	else
	{
		/*
		 * Possibly,
		 *   1. Need more input packets of this flow.
		 *   2. This flow/packet is not identified yet.
		 *   3. APPID is not activated, or there's no APPID request.
		 */
	}

	if (tdts_check_pkt_parameter_res(pkt_param, TDTS_RES_TYPE_DEVID))
	{
		PRT("DEVID -> %u,%u,%u,%u,%u,%u prio %u",
			TDTS_PKT_PARAMETER_RES_DEVID_VENDOR_ID(pkt_param),
			TDTS_PKT_PARAMETER_RES_DEVID_OS_NAME_ID(pkt_param),
			TDTS_PKT_PARAMETER_RES_DEVID_OS_CLASS_ID(pkt_param),
			TDTS_PKT_PARAMETER_RES_DEVID_DEV_CAT_ID(pkt_param),
			TDTS_PKT_PARAMETER_RES_DEVID_DEV_ID(pkt_param),
			TDTS_PKT_PARAMETER_RES_DEVID_DEV_FAMILY_ID(pkt_param),
			TDTS_PKT_PARAMETER_RES_DEVID_PRIO(pkt_param));
	}
}

#endif /* UBNT_NF_APP_DEBUG_H_ */
